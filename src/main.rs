use btleplug::api::{BDAddr, Central, CharPropFlags, Manager as _, Peripheral, ScanFilter};
use btleplug::platform::Manager;
use byteorder::{ByteOrder, LittleEndian};
use futures::stream::StreamExt;
use std::collections::HashMap;
use std::error::Error;
use std::time::Duration;
use std::{env, fmt, process};
use tokio::time;
use uuid::Uuid;

use clap::Parser;

extern crate pretty_env_logger;
#[macro_use]
extern crate log;

// influx
use chrono::{DateTime, Utc};
use influxdb::Client;
use influxdb::InfluxDbWriteable;

use std::string::ToString;
use strum_macros::Display;

// config
use dotenv::dotenv;

// regex
use regex::Regex;
#[derive(Parser, Debug)]
#[clap(
    author,
    version,
    about = "BLE scanner for reading data from BLE Xiaomi Thermometers. (--help for details)",
    long_about = "
BLE scanner for reading data from BLE Xiaomi Thermometers. Supported devices:
- LYWSD03MMC
- MJ_HT_V1
Config is done by env variables:
XIBLE_INFLUX_URL -> InfluxDB Endpoint (ie. http://127.0.0.1:8086)
XIBLE_INFLUX_DB -> InfluxDB Dabase name
XIBLE_SCAN_ATTEMPTS -> Number of scans if first doesn't find any device
XIBLE_SCAN_TIMEOUT -> How long wait for scan results

Result will be loaded into following tables & (fields):
- temperature (temperature, ble_mac)
- humidity (humidity, ble_mac)
- battery (battery, ble_mac) # for LYWSD03MMC battery is in Volts, for MJ_HT_V1 in %
"
)]
struct Args {
    /// Scan for devices but not db writes
    #[clap(short, long, value_parser)]
    test: bool,
}

// fn about() -> & 'static str {
//     "Hello moto"
// }

#[derive(Display)]
#[allow(non_camel_case_types)]
enum XiaomiThermometerModel {
    LYWSD03MMC,
    MJ_HT_V1,
    Unsupported,
}

struct XiaomiThermometer {
    mac: Option<BDAddr>,
    model: XiaomiThermometerModel,
    temperature: Option<f32>,
    humidity: Option<f32>,
    battery: Option<f32>,
    characteristic: Uuid,
}
impl fmt::Display for XiaomiThermometer {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "MAC:{} Model:{} Temperature: {:?}C Humidity:{:?}% Battery: {:?}V",
            self.mac
                .unwrap_or(BDAddr::from_str_delim("00:00:00:00:00:00").unwrap()),
            self.model,
            self.temperature,
            self.humidity,
            self.battery
        )
    }
}

#[derive(InfluxDbWriteable)]
struct TemperatureReading {
    time: DateTime<Utc>,
    temperature: f32,
    #[influxdb(tag)]
    ble_mac: String,
}

#[derive(InfluxDbWriteable)]
struct HumidityReading {
    time: DateTime<Utc>,
    humidity: i32,
    #[influxdb(tag)]
    ble_mac: String,
}
#[derive(InfluxDbWriteable)]
struct BatteryReading {
    time: DateTime<Utc>,
    battery: f32,
    #[influxdb(tag)]
    ble_mac: String,
}


impl XiaomiThermometer {
    pub fn new_from_model(model: &str) -> XiaomiThermometer {
        match model {
            "LYWSD03MMC" => XiaomiThermometer {
                mac: None,
                model: XiaomiThermometerModel::LYWSD03MMC,
                temperature: None,
                humidity: None,
                battery: None,
                characteristic: Uuid::from_u128(0xebe0ccc1_7a0a_4b0c_8a1a_6ff2997da3a6),
            },
            "MJ_HT_V1" => XiaomiThermometer {
                mac: None,
                model: XiaomiThermometerModel::MJ_HT_V1,
                temperature: None,
                humidity: None,
                battery: None,
                //characteristic: Uuid::from_u128(0x6d667044_7366_6275_6645_7664_55aa6c22),
                characteristic: Uuid::from_u128(0x226caa55_6476_4566_7562_66734470666d),
            },
            _ => XiaomiThermometer {
                mac: None,
                model: XiaomiThermometerModel::Unsupported,
                temperature: None,
                humidity: None,
                battery: None,
                characteristic: Uuid::nil(),
            },
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let cli = Args::parse();
    if cli.test {
        println!("Test mode enabled - no influxdb writing");
        if !env::var("RUST_LOG").is_ok() {
            env::set_var("RUST_LOG", "debug");
        }
    }
    dotenv().ok();
    pretty_env_logger::init();
    let mut processed_devices: HashMap<BDAddr, XiaomiThermometer> = HashMap::new();
    //test enviromenth
    let _influx_url = std::env::var("XIBLE_INFLUX_URL")
        .expect("XIBLE_INFLUX_URL must be set to Influx http endpoint");
    let _influx_db = std::env::var("XIBLE_INFLUX_DB").expect("XIBLE_INFLUX_DB must be set");

    //let supported_devices = vec!["MJ_HT_V1"];
    let supported_devices = vec!["LYWSD03MMC", "MJ_HT_V1"];
    let manager = Manager::new().await?;
    let adapter_list = manager.adapters().await?;
    if adapter_list.is_empty() {
        eprintln!("No Bluetooth adapters found");
    }

    for adapter in adapter_list.iter() {
        let mut scan_counter: u32 = 1;
        let scan_attempts = std::env::var("XIBLE_SCAN_ATTEMPTS")
            .unwrap_or("1".to_string())
            .parse::<u32>()
            .unwrap();
        let scan_timeout = std::env::var("XIBLE_SCAN_TIMEOUT")
            .unwrap_or("5".to_string())
            .parse::<u64>()
            .unwrap();
        'scan_loop: loop {
            if scan_counter > scan_attempts {
                println!(
                    "{} scans finished - {} devices found",
                    scan_counter - 1,
                    processed_devices.len()
                );
                break 'scan_loop
            }
            info!(
                "Starting scan attempt no. {} out of {}",
                &scan_counter, &scan_attempts
            );
            adapter
                .start_scan(ScanFilter::default())
                .await
                .expect("Can't scan BLE adapter for connected devices...");
            time::sleep(Duration::from_secs(scan_timeout)).await;
            let peripherals = adapter.peripherals().await?;

            if peripherals.is_empty() {
                eprintln!("->>> BLE peripheral devices were not found, sorry. Exiting...");
            } else {
                // All peripheral devices in range.
                'peripheral_loop: for peripheral in peripherals.iter() {
                    let properties = peripheral.properties().await?;
                    let is_connected = peripheral.is_connected().await?;
                    let local_name = properties
                        .unwrap()
                        .local_name
                        .unwrap_or(String::from("(unknown)"));
                    debug!(
                        "Peripheral {:?}({}) is connected: {:?}",
                        &local_name,
                        &peripheral.address(),
                        is_connected
                    );
                    // Check if it's the peripheral we want.
                    if supported_devices.contains(&local_name.as_str()) {
                        // if processed_devices.contains(&peripheral.address()) {
                        //     info!(
                        //         "Device {} already processed in this run, skipping",
                        //         &peripheral.address()
                        //     );
                        //     continue 'peripheral_loop;
                        // }
                        // make struct for received data
                        let thermometer = XiaomiThermometer::new_from_model(local_name.as_str());
                        processed_devices.insert(peripheral.address().clone(), thermometer);
                        println!(
                            "Found supported device {:?} ({})...",
                            &local_name,
                            peripheral.address()
                        );
                        if !is_connected {
                            // Connect if we aren't already connected.
                            if let Err(err) = peripheral.connect().await {
                                eprintln!("Error connecting to {} peripheral, skipping: {}", peripheral.address(), err);
                                continue;
                            }
                        }
                        let is_connected = peripheral.is_connected().await?;
                        debug!(
                            "Now connected ({:?}) to peripheral {:?}.",
                            is_connected, &local_name
                        );
                        if is_connected {
                            debug!("Discover peripheral {:?} services...", local_name);
                            peripheral.discover_services().await?;
                            'char_for: for characteristic in peripheral.characteristics() {
                                debug!("Checking characteristic {:?}", characteristic);
                                // Subscribe to notifications from the characteristic with the selected
                                // UUID.

                                // here we branch depending on model type
                                let thermometer: &mut XiaomiThermometer =
                                    processed_devices.get_mut(&peripheral.address()).unwrap();
                                match thermometer.model {
                                    XiaomiThermometerModel::LYWSD03MMC => {
                                        //if thermometer.model == XiaomiThermometerModel::LYWSD03MMC {
                                        if characteristic.uuid == thermometer.characteristic
                                            && characteristic
                                                .properties
                                                .contains(CharPropFlags::NOTIFY)
                                        {
                                            debug!(
                                                "Subscribing to characteristic {:?}",
                                                characteristic.uuid
                                            );
                                            peripheral.subscribe(&characteristic).await?;
                                            // Print the first 4 notifications received.
                                            let mut notification_stream =
                                                peripheral.notifications().await?.take(1);
                                            // Process while the BLE connection is not broken or stopped.
                                            while let Some(data) = notification_stream.next().await
                                            {
                                                let temp_raw = LittleEndian::read_u16(&data.value);
                                                let temp: f32 = temp_raw as f32 / 100 as f32;
                                                let humidity = &data.value[2];
                                                let battery_level_raw =
                                                    LittleEndian::read_u16(&data.value[3..]);
                                                let battery: f32 =
                                                    battery_level_raw as f32 / 1000 as f32;
                                                //         println!(
                                                //     "{} Temperatura: {:.1}, Wilgotność: {}%, Bateria: {:.2}V",peripheral.address(),
                                                //     temp, humidity, battery
                                                // );

                                                thermometer.mac = Some(peripheral.address());
                                                thermometer.temperature = Some(temp);
                                                thermometer.humidity = Some(humidity.clone() as f32);
                                                thermometer.battery = Some(battery);
                                                // let trm = XiaomiThermometer::newLYWSD03MMC(
                                                //     peripheral.address(),
                                                //     temp,
                                                //     humidity.clone(),
                                                //     battery,
                                                // );
                                                //processed_devices.push(peripheral.address());
                                            }
                                        }
                                    } // end of LYWSD03MMC match
                                    XiaomiThermometerModel::MJ_HT_V1 => {
                                        thermometer.mac = Some(peripheral.address());
                                        // battery first
                                        if characteristic.uuid
                                            == Uuid::from_u128(
                                                0x00002a19_0000_1000_8000_00805f9b34fb,
                                            )
                                            && characteristic
                                                .properties
                                                .contains(CharPropFlags::READ)
                                        {
                                            let battery_raw =
                                                peripheral.read(&characteristic).await;
                                            debug!("Battery raw value: {:?}", battery_raw);
                                            if let Ok(battery) = battery_raw {
                                                thermometer.battery =
                                                    Some(battery[battery.len() - 1] as f32);
                                            }
                                        }
                                        if characteristic.uuid == thermometer.characteristic
                                            && characteristic
                                                .properties
                                                .contains(CharPropFlags::NOTIFY)
                                        {
                                            debug!(
                                                "Subscribing to characteristic {:?}",
                                                characteristic.uuid
                                            );
                                            match peripheral.subscribe(&characteristic).await {
                                                Ok(()) => {}
                                                Err(e) => {
                                                    // for some reason on windows BLE stack from time to time
                                                    // there are problems with subsciption
                                                    warn!(
                                                        "Error subscribing to characteristic: {}",
                                                        e
                                                    );
                                                    continue 'char_for;
                                                }
                                            }
                                            let mut notification_stream =
                                                peripheral.notifications().await?.take(1);
                                            while let Some(rawdata) =
                                                notification_stream.next().await
                                            {
                                                debug!("MJ_HT_V1: Found required characteristic - trying to read");

                                                // //let mj_temp = peripheral.read(&characteristic).await;
                                                // println!("MJ_HT_V1 temp raw data: {:?}", rawdata);
                                                let data = String::from_utf8(rawdata.value)
                                                    .unwrap_or(String::from("NO DATA"));
                                                debug!("Raw data: {}", data);
                                                let re = Regex::new(r"^T=([+-]?[0-9]*[.]?[0-9]+) H=([+-]?[0-9]*[.]?[0-9]+).*").unwrap();
                                                if let Some(m) = re.captures(&data) {
                                                    debug!("regex match len: {}", m.len());
                                                    debug!("regex capture {:?}", m);
                                                    if let Ok(temp) =
                                                        m.get(1).unwrap().as_str().parse::<f32>()
                                                    {
                                                        thermometer.temperature = Some(temp);
                                                    };
                                                    if let Ok(h) =
                                                        m.get(2).unwrap().as_str().parse::<f32>()
                                                    {
                                                        thermometer.humidity = Some(h);
                                                    }
                                                    // println!(
                                                    //     "{} Temperatura: {:.1}, Wilgotność: {}%, Bateria: {:.2}V",peripheral.address(),
                                                    //     peripheral.address(), &thermometer.temperature, &thermometer.humidity
                                                    // );
                                                    //processed_devices.push(peripheral.address());
                                                    // if !cli.test {
                                                    //     println!("Sending to InfluxDB ..");
                                                    //     let _res =
                                                    //         push_to_influx(&thermometer).await;
                                                    // }
                                                } else {
                                                    warn!("No sensible data received from sensor");
                                                }
                                            }
                                        }
                                        // else {
                                        //     //println!("MJ_HT_V1 support work in progess")
                                        // }
                                    }
                                    XiaomiThermometerModel::Unsupported => {
                                        warn!("Unsupported device somehow added to supported list!! Ignoring")
                                    }
                                } // match goes here
                            } // characteristic scan end
                            debug!("Disconnecting from peripheral {:?}...", local_name);
                            peripheral.disconnect().await?;
                            continue 'peripheral_loop;
                        }
                    } else {
                        //break 'scan;
                        //println!("Skipping unknown peripheral {:?}", peripheral);
                    }
                } // peihperal scan end
            }
            scan_counter += 1;
        }
        for (key, value) in &processed_devices {
            println!(
                "{:?} Temperatura: {}, Wilgotność: {}%, Bateria: {}[V/%]",
                key, propt(value.temperature), propt(value.humidity), propt(value.battery)
            );
            if !cli.test { 
                let _ = push_to_influx(value).await;
            }
        }
        process::exit(0)
    }
    Ok(())
}

fn propt<T: std::fmt::Display>(v: Option<T>) -> String {
    if let Some(value) = v {
        format!("{}", value)
    } else {
        "".to_string()
    }
}
async fn push_to_influx(xt: &XiaomiThermometer) -> Result<(), Box<dyn Error>> {
    let influx_url = std::env::var("XIBLE_INFLUX_URL").unwrap();
    let influx_db = std::env::var("XIBLE_INFLUX_DB").unwrap();
    // Connect to db `test` on `http://localhost:8086`
    let client = Client::new(influx_url, influx_db); //.with_auth("admin", "3,14pi");
    let dt = Utc::now();
    if let Some(xt_mac) = xt.mac {
        if let Some(t) = xt.temperature {
            let temperature = TemperatureReading {
                time: dt,
                temperature: (t * 100.0).round() / 100.0, // round to 1 decimal value
                ble_mac: xt_mac.to_string(),
            };
            let write_result = client.query(temperature.into_query("temperature")).await;

            match write_result {
                Ok(_) => {
                    info!("{} IndfuxDB record for temperature saved successfully", propt(xt.mac));
                }
                Err(e) => {
                    warn!("{} Error inserting temperature record: {}",propt(xt.mac), e);
                }
            }

            // humidity
            if let Some(t) = xt.humidity {
                let humidity = HumidityReading {
                    time: dt,
                    humidity: t as i32, // round to 1 decimal value
                    ble_mac: xt_mac.to_string(),
                };
                let write_result = client.query(humidity.into_query("humidity")).await;
    
                match write_result {
                    Ok(_) => {
                        info!("{} IndfuxDB record for humidity saved successfully", propt(xt.mac));
                    }
                    Err(e) => {
                        warn!("{} Error inserting humidity record: {}",propt(xt.mac), e);
                    }
                }
            }

            //bettery
            
            // humidity
            if let Some(t) = xt.battery {
                let battery = BatteryReading {
                    time: dt,
                    battery: t , // round to 1 decimal value
                    ble_mac: xt_mac.to_string(),
                };
                let write_result = client.query(battery.into_query("battery")).await;
    
                match write_result {
                    Ok(_) => {
                        info!("{} IndfuxDB record for battery saved successfully", propt(xt.mac));
                    }
                    Err(e) => {
                        warn!("{} Error inserting battery record: {}",propt(xt.mac), e);
                    }
                }
            }
    
        } else {
            warn!("Device didn't have BLE MAC!! - not writing record to DB")
        }
    }
    Ok(())
}
