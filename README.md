# xible


Application for pulling temperature data from Xiaomi BLE themperature & humidity supported devices.
Currently application support following devices:
- MJ_HT_V1
- LYWSD03MMC



## Getting started


git clone <repo>
cargo build --release

xible --help:

```
BLE scanner for reading data from BLE Xiaomi Thermometers. Supported devices:
- LYWSD03MMC
- MJ_HT_V1
Config is done by env variables:
XIBLE_INFLUX_URL -> InfluxDB Endpoint (ie. http://127.0.0.1:8086)
XIBLE_INFLUX_DB -> InfluxDB Dabase name
XIBLE_SCAN_ATTEMPTS -> Number of scans if first doesn't find any device
XIBLE_SCAN_TIMEOUT -> How long wait for scan results

Result will be loaded into following tables & (fields):
- temperature (temperature, ble_mac)
- humidity (humidity, ble_mac)
- battery (battery, ble_mac) # for LYWSD03MMC battery is in Volts, for MJ_HT_V1 in %
```